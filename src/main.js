import Vue from 'vue'
import App from './App.vue'
import HomePage from './components/HomePage.vue'
import Router from "vue-router"
import ScheduleSelector from "./components/ScheduleSelector.vue"
import LoginPage from "./components/LoginPage.vue"
import GeneratedSchedulePage from "./components/GeneratedSchedulePage.vue"
import Axios from "axios"
import Fragment from "vue-fragment";

Axios.defaults.withCredentials = true;
console.log(process.env)
Axios.defaults.baseURL = (process.env.NODE_ENV !== "production") ? "http://localhost:8090" : process.env.VUE_APP_BACKEND_URL

Vue.config.productionTip = false

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Fragment)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Router);

const router = new Router({
	mode: "history",
	routes: [
		{
			path: "/",
			component: HomePage
		},
		{
			path: "/scheduleSelect",
			component: ScheduleSelector
		},
		{
			path: "/login",
			component: LoginPage
		},
		{
			path: "/scheduleGenerated/:id",
			component: GeneratedSchedulePage
		}
	]
})

new Vue({
	el: "#app",
	router,
	render: h => h(App)
})
