const alphabet = "abcdefghijklmnopqrstuvwxyz";
function PeriodParser({ totalPeriods, lunchPeriods }) {
    const parsePeriods = () => {
        var prevField = [];
        var letterIter = 0;
        var afterLunch = false;
        var afterLunchIter = 0;
        [...Array(totalPeriods)].map((_, i) => {
            if (lunchPeriods.indexOf(i) >= 0) {
                afterLunch = true;
                prevField = [...prevField, (`${i - lunchPeriods.indexOf(i) + 1}${alphabet[letterIter]}`)];
                letterIter++;
                afterLunchIter = i - lunchPeriods.indexOf(i) + 2;
            } else if (afterLunch) {
                // once it's lunch, use the afterLunchIterable.
                prevField = [...prevField, afterLunchIter++]
            } else {
                // if it's not after lunch, just start at 1 and go up.
                prevField = [...prevField, i + 1]
            }

            return prevField;
        })

        return prevField;
    }

    return parsePeriods();
}
export default PeriodParser